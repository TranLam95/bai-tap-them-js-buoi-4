// Start Bai 1

    
function homQua(){
    var textNgay = document.getElementById("txt-day").value*1;
    var textThang = document.getElementById("txt-month").value*1;
    var textNam = document.getElementById("txt-year").value*1;
    switch (textNgay){
        case 1: {
            if (textThang == 2 || textThang == 5){
                var ngayHomQua = 31;
                var thangTruoc = textThang - 1;
                var namTruoc = textNam;
            }else if (textThang == 10 || textThang == 12){
                var ngayHomQua = 31;
                var thangTruoc = textThang - 1;
                var namTruoc = textNam;
            } else if (textThang == 1){
                var ngayHomQua = 31;
                var thangTruoc = 12;
                var namTruoc = textNam -1;
            }
            break;
        }
        default: {
            var ngayHomQua = textNgay - 1;
            var thangTruoc = textThang;
            var namTruoc = textNam;
            break;
        }
    }
    document.getElementById("result1").innerHTML = `<p>Ngày hôm qua là: ${ngayHomQua}/${thangTruoc}/${namTruoc}`
}

function ngayMai(){
    var textNgay = document.getElementById("txt-day").value*1;
    var textThang = document.getElementById("txt-month").value*1;
    var textNam = document.getElementById("txt-year").value*1;
    switch (textNgay){
        case 28: {
            if (textThang == 2){
                var ngayHomsau = 1;
                var thangSau = textThang + 1;
                var namSau = textNam;
            } else {
                var ngayHomsau = textNgay + 1;
                var thangSau = textThang;
                var namSau = textNam;
            }
        }
        case 31: {
            if (textThang == 1 || textThang == 3){
                var ngayHomsau = 1;
                var thangSau = textThang + 1;
                var namSau = textNam;
            } else if (textThang == 5 || textThang == 7){
                var ngayHomsau = 1;
                var thangSau = textThang + 1;
                var namSau = textNam;
            } else if (textThang == 8 || textThang == 10){
                var ngayHomsau = 1;
                var thangSau = textThang + 1;
                var namSau = textNam;
            } else if(textThang == 12){
                var ngayHomsau = 1;
                var thangSau = 1;
                var namSau = textNam + 1;
            }
            break;
        }
        default: {
            var ngayHomQua = textNgay + 1;
            var thangSau = textThang;
            var namSau = textNam;
            break;
        }
    }
    document.getElementById("result1").innerHTML = `<p>Ngày mai là: ${ngayHomsau}/${thangSau}/${namSau}`
}

// End Bai 1
// Start Bai 2
function tinhNgay(){
    var nhapThang = document.getElementById("txt-thang").value;
    var nhapNam  = document.getElementById("txt-nam").value*1;
    if (nhapThang == "Tháng 1"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 2" && nhapNam%4==0){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 29 ngày`;
    } else if (nhapThang == "Tháng 2" && nhapNam%4==1){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 28 ngày`;
    } else if (nhapThang == "Tháng 3"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 4"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 30 ngày`;
    } else if (nhapThang == "Tháng 5"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 6"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 30 ngày`;
    } else if (nhapThang == "Tháng 7"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 8"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 9"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 30 ngày`;
    } else if (nhapThang == "Tháng 10"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    } else if (nhapThang == "Tháng 11"){
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 30 ngày`;
    } else {
        document.getElementById("result2").innerHTML = `${nhapThang} năm ${nhapNam} có 31 ngày`;
    }
}

// End Bai 2
// Start Bài 3
function docSo() {
    var number = document.getElementById("txt-number").value*1;
    var hangTram = Math.floor(number/100);
    var hangChuc = Math.floor(number%100/10);
    var hangDV = number%10;
    switch (hangTram){
        case 0:
            var textHangTram = "";
            break;
        case 1:
            var textHangTram = "Một trăm ";
            break;
        case 2:
            var textHangTram = "Hai trăm ";
            break;
        case 3:
            var textHangTram = "Ba trăm ";
            break;
        case 4:
            var textHangTram = "Bốn trăm ";
            break;
        case 5:
            var textHangTram = "Năm trăm ";
            break;
        case 6:
            var textHangTram = "Sáu trăm ";
            break;
        case 7:
            var textHangTram = "Bảy trăm ";
            break;
        case 8:
            var textHangTram = "Tám trăm ";
            break;
        case 9:
            var textHangTram = "Chín trăm ";
            break;
    }
    switch (hangChuc){
        case 0:
            var textHangChuc = "";
            break;
        case 1:
            var textHangChuc = "mười ";
            break;
        case 2:
            var textHangChuc = "hai mươi ";
            break;
        case 3:
            var textHangChuc = "ba mươi ";
            break;
        case 4:
            var textHangChuc = "bốn mươi ";
            break;
        case 5:
            var textHangChuc = "năm mươi ";
            break;
        case 6:
            var textHangChuc = "sáu mươi ";
            break;
        case 7:
            var textHangChuc = "bảy mươi ";
            break;
        case 8:
            var textHangChuc = "tám mươi ";
            break;
        case 9:
            var textHangChuc = "chín mươi ";
            break;
    }
    switch (hangDV){
        case 0:
            var textHangDV = "";
            break;
        case 1:
            var textHangDV = "một";
            break;
        case 2:
            var textHangDV = "hai";
            break;
        case 3:
            var textHangDV = "ba";
            break;
        case 4:
            var textHangDV = "bốn";
            break;
        case 5:
            var textHangDV = "năm";
            break;
        case 6:
            var textHangDV = "sáu";
            break;
        case 7:
            var textHangDV = "bảy";
            break;
        case 8:
            var textHangDV = "tám";
            break;
        case 9:
            var textHangDV = "chín";
            break;
    }
    document.getElementById("result3").innerHTML = `${textHangTram} ${textHangChuc} ${textHangDV}`;
}
// End Bài 3
// Start Bài 4
function ketQua(){
    var tenSV1 = document.getElementById("txt-sinh-vien1").value;
    var toaDoX1 = document.getElementById("txt-x1").value*1;
    var toaDoY1 = document.getElementById("txt-y1").value*1;
    var tenSV2 = document.getElementById("txt-sinh-vien2").value;
    var toaDoX2 = document.getElementById("txt-x2").value*1;
    var toaDoY2 = document.getElementById("txt-y2").value*1;
    var tenSV3 = document.getElementById("txt-sinh-vien3").value;
    var toaDoX3 = document.getElementById("txt-x3").value*1;
    var toaDoY3 = document.getElementById("txt-y3").value*1;
    var toaDoTruongX = document.getElementById("txt-x4").value*1;
    var toaDoTruongY = document.getElementById("txt-y4").value*1;
    var khoangCach1 = Math.sqrt((Math.pow(toaDoTruongX - toaDoX1,2)) + (Math.pow(toaDoTruongY - toaDoY1,2)),2);
    var khoangCach2 = Math.sqrt((Math.pow(toaDoTruongX - toaDoX2,2)) + (Math.pow(toaDoTruongY - toaDoY2,2)),2);
    var khoangCach3 = Math.sqrt((Math.pow(toaDoTruongX - toaDoX3,2)) + (Math.pow(toaDoTruongY - toaDoY3,2)),2);
    if (khoangCach1>khoangCach2 && khoangCach1>khoangCach3){
        document.getElementById("result4").innerHTML = `<p> Sinh viên xa trường nhất là ${tenSV1}`;
    } else if (khoangCach2>khoangCach1 && khoangCach2>khoangCach3){
        document.getElementById("result4").innerHTML = `<p> Sinh viên xa trường nhất là ${tenSV2}`;
    } else {
        document.getElementById("result4").innerHTML = `<p> Sinh viên xa trường nhất là ${tenSV3}`;
    }
}

// End Bài 4